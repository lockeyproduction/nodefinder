const ethers = require('ethers');
const web3 = require('web3');
const config = require('./config.json');
const contract = require('./utils/contract');
const mongoose = require("mongoose");
const axios = require('axios');
const Coin = require('./models/coin.model');
const telegram = require('./utils/telegram');
const Web3 = new web3(new web3.providers.HttpProvider('https://bsc-dataseed.binance.org/'));
const addresses = {
    WBNB: ethers.utils.getAddress('0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c'),
    factory: ethers.utils.getAddress('0xcA143Ce32Fe78f1f7019d7d551a6402fC5350c73'),
    router: ethers.utils.getAddress('0x10ED43C718714eb63d5aA57B78B54704E256024E'),
};

const provider = new ethers.providers.JsonRpcProvider(config.mainNet);
const wallet = new ethers.Wallet(config.privateKey);
const account = wallet.connect(provider);

const factory = new ethers.Contract(
    addresses.factory,
    ['event PairCreated(address indexed token0, address indexed token1, address pair, uint)'],
    account
);

const getTransactions = async (address) => {
    const latest = await Web3.eth.getBlockNumber()
    let url = "https://api.bscscan.com/api?module=account&action=txlist&address=" + address + "&startblock=0&endblock=" + latest + "&sort=asc&apiKey=P4RVE9X2NB1ND3EP1EN586T4Y211HDX1VW"
    const response = await axios.get(url)
    let transactions = response.data.result

    while (transactions.length === 0) {
        const response = await axios.get(url)
        transactions = response.data.result
    }

    return transactions
};

const checkTransactionsForNodeTokens = async (tokenOut, tokenOutName, transactions, token0 = null, token1 = null, pairAddress = null) => {
    for (const transaction of transactions) {
        if (transaction.hasOwnProperty("contractAddress") && transaction.contractAddress !== '') {
            console.log(transaction.contractAddress)
            console.log(`
                        New pair detected
                        ===============================================================================
                        tokenName: ${tokenOutName}
                        token0: ${token0}
                        token1: ${token1}
                        pairAddress: ${pairAddress}
                        creator: ${transaction.from}
                   `);

            const creatorTransactions = await getTransactions(transaction.from)
            for (const creatorTx of creatorTransactions) {
                if (creatorTx.hasOwnProperty("contractAddress") && creatorTx.contractAddress !== '') {
                    console.log(`
                                Creator transaction detected
                                =================
                                contractAddress: ${creatorTx.contractAddress}
                                transactionHash: ${creatorTx.hash}
                           `);
                    let bytecode = await Web3.eth.getCode(ethers.utils.getAddress(creatorTx.contractAddress))
                    if (bytecode.includes("54557973") || bytecode.includes("65bfe430") || bytecode.includes("d766636b") || bytecode.includes("f74c9934")) {
                        let coin = {tokenAddress: tokenOut};
                        console.log(coin)
                        let newCoin = await Coin.create(coin);
                        console.log(newCoin)
                        await telegram.sendMessage(tokenOut)
                    }else{
                        console.log("Not a node token")
                    }
                }
            }
            console.log("==============================================================================")
        }
    }
}

(async () => {
    try {
        await mongoose.connect("mongodb://127.0.0.1:27017/node", {})
        console.log('Connected to MongoDB');
        //let token = "0x871977a373DD1e26E24179d755fE2a1713059A75" //Lannister
        //let token = "0x521ef54063148e5f15f18b9631426175cee23de2" // RING
        //let token = "0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c" //BNB
        //let token = "0xce82d1504d22ecf4eab3b356cc0eb63f31e2a38c" // BANANAZ
        //let token = "0x2d3d0ed0019f4e7ce99aca6ad38eb3ee2bf67ad6" //islands manager
        //let token = "0x247f7ad24a2a21ba3ae475fb0114528c916ea3e4"//metaisland
        //let token = "0xe2bc3c5c8d590f83d8916549533e8d52f68c2049" //gold manager
        //let token = "0x57c00ba3a138663ee1e5b1d3cd40003f0fa0477d" //random labiacoin
        //let Web3 = new web3(new web3.providers.HttpProvider(config.mainNet));
        //let bytecode = await Web3.eth.getCode(token)
        //console.log(bytecode)
        //let token = "0x521ef54063148e5f15f18b9631426175cee23de2" // RING
        //const transactions = await getTransactions(token)
        //await checkTransactionsForNodeTokens(token, "test", transactions)

        factory.on('PairCreated', async (token0, token1, pairAddress) => {

            token0 = ethers.utils.getAddress(token0);
            token1 = ethers.utils.getAddress(token1);
            let tokenIn, tokenOut;

            if (token0 === addresses.WBNB) {
                tokenIn = token0;
                tokenOut = token1;
            }

            if (token1 === addresses.WBNB) {
                tokenIn = token1;
                tokenOut = token0;
            }
            console.log('here', token0, token1)


            let tokenInName = await contract.getTokenName(tokenIn);
            let tokenOutName = await contract.getTokenName(tokenOut);
            console.log(tokenInName, tokenOutName)


            const transactions = await getTransactions(tokenOut)
            //console.log(transactions)
            await checkTransactionsForNodeTokens(tokenOut, tokenOutName, transactions, token0, token1, pairAddress)


        });

    } catch (err) {
        console.log(err)
        await telegram.sendMessage(err.toString())
    }


})();




