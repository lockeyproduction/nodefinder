const ethers = require('ethers');
const web3 = require('web3');
const config = require('./config.json');
const contract = require('./utils/contract');
const mongoose = require("mongoose");
const axios = require('axios');
const Coin = require('./models/coin.model');
const telegram = require('./utils/telegram');
const addresses = {
    WBNB: ethers.utils.getAddress('0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c'),
    factory: ethers.utils.getAddress('0xcA143Ce32Fe78f1f7019d7d551a6402fC5350c73'),
    router: ethers.utils.getAddress('0x10ED43C718714eb63d5aA57B78B54704E256024E'),
};

const provider = new ethers.providers.JsonRpcProvider(config.mainNet);
const wallet = new ethers.Wallet(config.privateKey);
const account = wallet.connect(provider);



(async () => {
    try {
            let Web3 = new web3(new web3.providers.HttpProvider('https://bsc-dataseed.binance.org/'));
            const latest = await Web3.eth.getBlockNumber()
            let url = "https://api.bscscan.com/api?module=account&action=txlist&address=" + "0x1D5e62783B4C2AC30c26033CFd47DEBD9965B090" + "&startblock=0&endblock=" + latest + "&sort=asc&apiKey=P4RVE9X2NB1ND3EP1EN586T4Y211HDX1VW"
            const response = await axios.get(url)
            let transactions = response.data.result
            while(transactions.length === 0){
                const response = await axios.get(url)
                transactions = response.data.result
            }
            for(const transaction of transactions){
                if(transaction.hasOwnProperty("contractAddress") && transaction.contractAddress !== ''){
                    console.log(transaction.contractAddress)
                }
            }

    } catch (err) {
        console.log(err)
        await telegram.sendMessage(err.toString())
    }


})();




