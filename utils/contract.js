const ethers = require('ethers');
const { dirname } = require('path');
const appDir = dirname(require.main.filename);
const ERC20 = require(appDir + '/abi/erc20.json');
const config = require("../config.json");

const provider = new ethers.providers.JsonRpcProvider(config.mainNet);
const signer = new ethers.Wallet(config.privateKey, provider)

async function getTokenName(address){
    const tokenContract = await getFreeContractInstance(address, ERC20)
    const tokenName = await callContractMethod(tokenContract, "name")
    return tokenName
}



async function checkRewardToken(address){
    let isReward = false;
    const tokenContract = await getFreeContractInstance(address, ERC20)

    isReward = await callContractMethod(tokenContract, "dividendTracker")
    console.log(isReward)

    return isReward
}

async function checkNodeToken(address){
    let isNode = false;
    const tokenContract = await getFreeContractInstance(address, ERC20)
    isNode = await callContractMethod(tokenContract, "getNodePrice")
    console.log(isNode)
    return isNode
}

async function callContractMethod(contractInstance, methodName, options = {}){
    let resultOfCall = null
    try{
        switch(methodName){
            case "balanceOf":
                resultOfCall = await contractInstance[methodName](options)
                break;
            case "createNodeWithTokens":
                resultOfCall = await contractInstance[methodName](options)
                break;
            case "getNodeNumberOf":
                console.log(options)
                resultOfCall = await contractInstance[methodName](options)
                break;
            default:
                resultOfCall = await contractInstance[methodName]()
                break;
        }
    }catch(err){
        console.log('error', methodName, options)
        console.log(err)
        if(err.code === "UNPREDICTABLE_GAS_LIMIT"){
            return true
        }

        return false
    }

    return resultOfCall
}

async function getFreeContractInstance(contractAddress, abi, signerOrProvider = signer){
    const contract = new ethers.Contract(contractAddress, abi, signerOrProvider)
    return contract
}

module.exports = {
    getTokenName,
    callContractMethod,
    getFreeContractInstance,
    checkNodeToken,
    checkRewardToken
}